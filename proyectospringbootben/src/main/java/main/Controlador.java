/**
 * @author Daniel Benjamin Rigaud  |  0013-3655
 */
package main;

import java.io.IOException;
import org.jsoup.Jsoup;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@Controller
@EnableAutoConfiguration
public class Controlador {
    
    //Mapeo de la url principal
    @RequestMapping("/")
    @ResponseBody
    String home() {
        return "<h1>¡Hola mundo!</h1><p>Daniel Benjamin Rigaud  |  0013-3655</p>";
    }
    
    //Mapeo de la url para el ejemplo de jsoup
    @RequestMapping("/jsoup")
    @ResponseBody
    String jsoup() throws IOException {
        
        // Le doy a jsoup la url que quiero que tome, en este caso / y pongo su contenido en la variable contenido
        String contenido = Jsoup.connect("http://127.0.0.1:8080/").get().text();
        // Imprimo la variable contenido
        return "<h3>Abajo esta el contenido de el url (/) traido como texto con jsoup</h3>" +contenido;
    }
    
    public static void main(String[] args) throws Exception {
        // Corre Spring Boot
        SpringApplication.run(Controlador.class, args);
    }
}
