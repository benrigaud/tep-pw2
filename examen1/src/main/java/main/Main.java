/**
 * @author Daniel Benjamin Rigaud  |  0013-3655
 */
package main;

import edu.pucmm.Parcial;
import edu.pucmm.domains.Student;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@Controller
@EnableAutoConfiguration
public class Main {

    @RequestMapping("/punto2")
    @ResponseBody
    String puntodos() throws IOException {
        // Le doy a jsoup la url que quiero que tome, en este caso / y pongo su contenido en la variable contenido
        String contenido = Jsoup.connect("http://cloudlott.com:8070/exam/234556").get().text();
        // Imprimo la variable contenido
        return contenido;
    }
    
    @RequestMapping("/lista")
    @ResponseBody
    String lista(){
        List<Student> students = Parcial.get().getStudents();
        
        // Imprimir todos los estudiantes----------------------------------------------
        String todos = Parcial.get().getStudents().toString();
        
        // Buscar estudiantes para borrar
        List<Student> toDelete = new ArrayList<>();
        for (Student student : students) {
            if (student.getNames().toLowerCase().contains(Parcial.get().getExpToDelete() + "")) {
                toDelete.add(student);
            }
        }
        // Borrar-----------------------------------------------------------------------
        Parcial.get().getStudents().removeAll(toDelete);
        // Asignar valores a variables
        int debieronSerEliminados = Parcial.get().getStudentsDelete();
        int fueronEliminados = toDelete.size();
        
        // Buscar estudiantes para listar-----------------------------------------------
        List<Student> toCount = new ArrayList<>();
        for (Student student : students) {
            if (student.getNames().toLowerCase().contains(Parcial.get().getExpToCount() + "")) {
                toCount.add(student);
            }
        }
        // Asignar valores a variables
        int debieronSerContados = Parcial.get().getStudentsCount();
        int fueronContados = toCount.size();
        
        // Imprimir
        return "Debieron ser eliminados: " +debieronSerEliminados+ " Fueron eliminados: " +fueronEliminados+ "<br>Debieron ser contados: " +debieronSerContados+ " Fueron contados: " +fueronContados+ "<br>Todos los estudiantes: " +todos;
    }
 

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Main.class, args);
    }
}