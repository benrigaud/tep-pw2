/**
 * @author Daniel Benjamin Rigaud  |  0013-3655
 */
package main;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        
        // Crear lista
        List<Estudiantes> estudiantes = new ArrayList<>();
        estudiantes.add(new Estudiantes("Jose", "Pérez", "15-07-1989"));
        estudiantes.add(new Estudiantes("Juan", "López", "2-02-1989"));
        estudiantes.add(new Estudiantes("María", "Fernández", "10-11-1988"));
        estudiantes.add(new Estudiantes("Miguel", "Cruz", "7-06-1990"));
        estudiantes.add(new Estudiantes("Antonio", "Martínez", "11-03-1990"));
        estudiantes.add(new Estudiantes("Luis", "García", "22-10-1987"));
        estudiantes.add(new Estudiantes("Ramón", "Ruíz", "17-09-1989"));
        // Imprimir lista
        System.out.println("Lista original");
        System.out.println("__________________________________");
        for (Estudiantes estudiante : estudiantes) {
            System.out.println(estudiante.todo());
        }
        // Ordeno e imprimo en base a nombre
        System.out.println("");
        System.out.println("Lista ordenada por nombres");
        System.out.println("__________________________________");
        sortAndPrint(estudiantes, new Comparator<Estudiantes>() {
            @Override
            public int compare(Estudiantes o1, Estudiantes o2) {
               return o1.getNombre().toLowerCase().compareTo(o2.getNombre().toLowerCase());
            }
        });
        // Seleccionar para borrar
        List<Estudiantes> toDelete = new ArrayList<>();
        for (Estudiantes estudiante : estudiantes) {
            if (estudiante.getNombre().toLowerCase().contains("a")) {
                toDelete.add(estudiante);
            }
        }
        // Borrar seleccionados
        estudiantes.removeAll(toDelete);
        // Imprimir lista
        System.out.println("");
        System.out.println("Lista con todos los estudiantes con A en el nombre eliminados");
        System.out.println("__________________________________");
        for (Estudiantes estudiante : estudiantes) {
            System.out.println(estudiante.todo());
        }
  
    }
    // Metodo para comparar
    private static void sortAndPrint(List<Estudiantes> estudiantes, Comparator<Estudiantes> comparator) {
        estudiantes.sort(comparator);
        for (Estudiantes estudiante : estudiantes) {
            System.out.println(estudiante.todo());
        }
    }
}

//        // Ordeno e imprimo en base a numero
//        System.out.println("Ordeno en base al indice");
//        sortAndPrint(students, new Comparator<Student>() {
//            @Override
//            public int compare(Student o1, Student o2) {
//                if (o1.getId() < o2.getId()) {
//                    return 1;
//                } else if (o1.getId() > o2.getId()) {
//                    return -1;
//                }
//                return 0;
//            }
//        });