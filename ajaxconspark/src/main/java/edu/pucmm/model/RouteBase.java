package edu.pucmm.model;

import spark.Route;

/**
 * @author benrigaud | 0013-3655
 */

public abstract class RouteBase implements Route {

    public String path;

    protected RouteBase(String path) {
        this.path = path;
    }
}
