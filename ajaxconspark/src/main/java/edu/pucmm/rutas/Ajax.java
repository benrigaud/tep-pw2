package edu.pucmm.rutas;

import edu.pucmm.model.RouteBase;
import spark.Request;
import spark.Response;
import java.io.File;
import java.nio.file.Files;

/**
 * @author benrigaud | 0013-3655
 */

public class Ajax extends RouteBase {

    public Ajax() {
        super("/ajax");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        return new String(Files.readAllBytes(new File("src/main/resources/html/ajax.html").toPath()));
    }
}
