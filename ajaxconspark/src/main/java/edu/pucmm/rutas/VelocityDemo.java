package edu.pucmm.rutas;

import edu.pucmm.model.RouteBase;
import java.io.StringWriter;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import spark.Request;
import spark.Response;

/**
 * @author benrigaud | 0013-3655
 */

public class VelocityDemo extends RouteBase {

    public VelocityDemo() {
        super("/velocity");
    }
    
    private static final VelocityEngine velocityEngine = new VelocityEngine();

    @Override
    public Object handle(Request request, Response response) throws Exception {
        velocityEngine.init();
        Template template = velocityEngine.getTemplate("html/velocitydemo.html");
        VelocityContext context = new VelocityContext();
        context.put("name", "Benjamin Rigaud");
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }
}
