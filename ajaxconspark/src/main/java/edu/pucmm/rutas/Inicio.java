package edu.pucmm.rutas;

import edu.pucmm.model.RouteBase;
import spark.Request;
import spark.Response;

/**
 * @author benrigaud | 0013-3655
 */

public class Inicio extends RouteBase {

    public Inicio() {
        super("/");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        return "¡Hola mundo!";
    }
}
