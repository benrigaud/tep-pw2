package edu.pucmm;

import edu.pucmm.rutas.Inicio;
import edu.pucmm.model.RouteBase;
import edu.pucmm.rutas.Ajax;
import edu.pucmm.rutas.VelocityDemo;
import spark.Spark;

/**
 * @author benrigaud | 0013-3655
 */

public class Main {

    public static void main(String[] args) {
        Spark.port(8070);
        addRoute(new Inicio());
        addRoute(new Ajax());
        addRoute(new VelocityDemo());
        Spark.init();
    }

    private static void addRoute(RouteBase routeBase) {
        Spark.get(routeBase.path, routeBase);
    }
}
