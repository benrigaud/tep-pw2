package edu.pucmm;

import edu.pucmm.utils.Constants;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class DataCreator {

    // TODO utilizar la serializacion en este caso puede ser más rapido, pero menos entendible al estudiante

    /**
     * Para usar debo tener el archivo a escribir y paso el objeto a escribir en el y dejo que el API funcione.
     *
     * @param file   Destino a escribir
     * @param object Objecto de instancia de clase con valores
     * @return Si escribió o no el objeto
     */
    public boolean saveDataToFile(File file, Object object) {
        try {
            PrintWriter printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
            printWriter.println(Constants.get().stringify(object));
            printWriter.flush();
            printWriter.close();
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    /**
     * Lee el archivo completo con la data del tipo de clase.
     *
     * @param file   Archivo a leer
     * @param tClass Tipo de clase del objeto a leer
     * @param <T>    Clase del objeto
     * @return Listado del objeto
     */
    public <T> List<T> readData(File file, Class<T> tClass) {
        try {
            List<T> data = new ArrayList<>();
            for (String line : Files.readAllLines(file.toPath())) {
                data.add(Constants.get().convert(line, tClass));
            }
            return data;
        } catch (IOException ignored) {
            return new ArrayList<>();
        }
    }
}
