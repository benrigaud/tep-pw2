package org.pucmm.rutas;

import java.io.StringWriter;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.pucmm.model.Item;
import org.pucmm.utils.DataCreator;
import org.pucmm.model.RouteBase;
import org.pucmm.utils.Constants;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class Store extends RouteBase {

    public Store() {
        super("/store");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init();
        Template template = velocityEngine.getTemplate("html/store.html");
        VelocityContext context = new VelocityContext();
        context.put("items", DataCreator.readData(Constants.DATA_FILE, Item.class));
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }

}
