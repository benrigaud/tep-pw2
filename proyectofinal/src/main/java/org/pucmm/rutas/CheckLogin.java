package org.pucmm.rutas;

import org.pucmm.model.RouteBase;
import org.pucmm.utils.Constants;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class CheckLogin extends RouteBase {

    private static String USER = "ben";
    private static String PASS = "123";

    public CheckLogin() {
        super("/check/login");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        String user = request.queryParams("user");
        String pass = request.queryParams("pass");
        if (user.equals(USER) && pass.equals(PASS)) {
            response.cookie("/", Constants.COOKIE_NAME, Constants.COOKIE_TRUE, 3600, false, true);
            return "1";
        }
        return "0";
    }

}
