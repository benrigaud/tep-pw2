package org.pucmm.rutas;

import org.pucmm.model.Item;
import org.pucmm.model.RouteBase;
import org.pucmm.utils.Constants;
import org.pucmm.utils.DataCreator;
import java.util.List;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class AddToCar extends RouteBase {
    
    public static final String SEPARATOR = "_";

    public AddToCar() {
        super("/add/car");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        try {
            String cookie = request.cookie(Constants.COOKIE_CAR_NAME);
            if (cookie == null) {
                cookie = "";
            }
            String item = request.queryParams("item");
            String cant = request.queryParams("cant");
            long itemID = -1;
            int cantNum = -1;
            try {
                itemID = Long.parseLong(item);
                cantNum = Integer.parseInt(cant);
            } catch (Exception ignored) {
            }
            if (itemID != -1 && cantNum != -1) {
                cookie = addItem(cookie, itemID, cantNum);
                response.cookie("/", Constants.COOKIE_CAR_NAME, cookie, -1, false, true);
                return "1";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "0";
    }

    private String addItem(String cookie, Long itemID, int cantNum) {
        if (cookie.isEmpty()) {
            cookie = itemID + ":" + cantNum + SEPARATOR;
        } else {
            boolean exist = false;
            String[] parts = cookie.split(SEPARATOR);
            for (int i = 0; i < parts.length; i++) {
                String item = parts[i].split(":")[0];
                int cant = 0;
                try {
                    cant = Integer.parseInt(parts[i].split(":")[1]);
                } catch (Exception ignored) {
                }
                List<Item> items = DataCreator.readData(Constants.DATA_FILE, Item.class);
                if (item.equals(itemID.toString())) {
                    parts[i] = itemID + ":" + (cantNum + cant) + SEPARATOR;
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                cookie += itemID + ":" + cantNum + SEPARATOR;
            } else {
                cookie = "";
                for (String part : parts) {
                    cookie += part;
                }
            }
        }
        return cookie;
    }
}
