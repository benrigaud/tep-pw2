package org.pucmm.rutas;

import java.io.StringWriter;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.pucmm.model.RouteBase;
import org.pucmm.utils.Constants;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class Logout extends RouteBase {

    public Logout() {
        super("/logout");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        response.cookie("/", Constants.COOKIE_NAME, Constants.COOKIE_FALSE, 3600, false, true);
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init();
        Template template = velocityEngine.getTemplate("html/login.html");
        VelocityContext context = new VelocityContext();
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }

}