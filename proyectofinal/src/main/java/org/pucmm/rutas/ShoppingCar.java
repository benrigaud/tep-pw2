package org.pucmm.rutas;

import org.pucmm.model.Item;
import org.pucmm.model.RouteBase;
import org.pucmm.utils.Constants;
import org.pucmm.utils.DataCreator;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import spark.Request;
import spark.Response;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ShoppingCar extends RouteBase {

    public ShoppingCar() {
        super("/shopping-cart");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        try {
            Template template = velocityEngine.getTemplate("html/shopping_cart.html");
            VelocityContext context = new VelocityContext();
            List<Item> itemsOnDisk = DataCreator.readData(Constants.DATA_FILE, Item.class);
            List<Item> toShow = new ArrayList<>();
            String cookie = request.cookie(Constants.COOKIE_CAR_NAME);
            if (cookie == null) {
                cookie = "";
            }
            for (String part : cookie.split(AddToCar.SEPARATOR)) {
                long itemID = -1;
                int cantNum = -1;
                try {
                    itemID = Long.parseLong(part.split(":")[0]);
                    cantNum = Integer.parseInt(part.split(":")[1]);
                } catch (Exception ignored) {
                }
                Item itemFound = findItem(itemsOnDisk, itemID);
                if (itemFound != null) {
                    itemFound.setCant(cantNum);
                    toShow.add(itemFound);
                }
            }
            context.put("items", toShow);
            StringWriter writer = new StringWriter();
            template.merge(context, writer);
            return writer.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    private Item findItem(List<Item> itemsOnDisk, Long id) {
        for (Item item : itemsOnDisk) {
            if (Objects.equals(item.getId(), id)) {
                return item;
            }
        }
        return null;
    }
}
