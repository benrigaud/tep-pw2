package org.pucmm.rutas;

import org.pucmm.model.Item;
import org.pucmm.model.RouteBase;
import org.pucmm.utils.Constants;
import org.pucmm.utils.DataCreator;
import java.util.ArrayList;
import java.util.List;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class Pay extends RouteBase {

    public Pay() {
        super("/pay");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        boolean canPay = false;
        String cookie = request.cookie(Constants.COOKIE_CAR_NAME);
        if (cookie == null) {
            cookie = "";
        }
        try {
            canPay = Integer.parseInt(request.queryParams("canPay")) > 0;
        } catch (Exception ignored) {
        }
        if (canPay) {
            List<Item> items = DataCreator.readData(Constants.DATA_FILE, Item.class);
            for (String part : cookie.split(AddToCar.SEPARATOR)) {
                long itemID = -1;
                int cantNum = -1;
                try {
                    itemID = Long.parseLong(part.split(":")[0]);
                    cantNum = Integer.parseInt(part.split(":")[1]);
                } catch (Exception ignored) {
                }
                for (Item item : items) {
                    if (item.getId() == itemID) {
                        item.setCant(item.getCant() - cantNum);
                        break;
                    }
                }
            }
            List<Item> toDelete = new ArrayList<>();
            for (Item item : items) {
                if (item.getCant() <= 0) {
                    toDelete.add(item);
                }
            }
            items.removeAll(toDelete);
            DataCreator.replaceDataToFile(Constants.DATA_FILE, items);
            response.cookie("/", Constants.COOKIE_CAR_NAME, "", -1, false, true);
            return "1";
        }
        return "0";
    }

}