package org.pucmm.filters;

import org.pucmm.utils.Constants;
import spark.Filter;
import spark.Request;
import spark.Response;

public class CredentialFilter implements Filter {


    @Override
    public void handle(Request request, Response response) throws Exception {
        String cookie = request.cookie(Constants.COOKIE_NAME);
        if (cookie == null || cookie.equals(Constants.COOKIE_FALSE)) {
            if (!request.pathInfo().equals("/login") && !request.pathInfo().equals("/check/login")) {
                response.redirect("/login");
            }
        }
    }
}
