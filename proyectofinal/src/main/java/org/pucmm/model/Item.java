package org.pucmm.model;

public class Item {

    private String image = "";
    
    private Long id;
    private String name;
    private int cant;
    private double price;

    public Item() {
    }

    public Item(Long id, String name, int cant, double price) {
        this.id = id;
        this.name = name;
        this.cant = cant;
        this.price = price;
    }

    public Item(String image, Long id, String name, int cant, double price) {
        this.image = image;
        this.id = id;
        this.name = name;
        this.cant = cant;
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
