package org.pucmm.model;

import org.apache.velocity.app.VelocityEngine;
import spark.Route;

public abstract class RouteBase implements Route {

    protected VelocityEngine velocityEngine = new VelocityEngine();
    public String path;

    protected RouteBase(String path) {
        this.path = path;
    }
}
